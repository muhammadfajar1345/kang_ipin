import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:kang_ipin/model.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_linkify/flutter_linkify.dart';

class Maindashboard extends StatefulWidget {
  @override
  _MaindashboardState createState() => _MaindashboardState();
}

class _MaindashboardState extends State<Maindashboard> {
  GlobalSembuh globalSembuh;
  GlobalSakit globalSakit;
  GlobalMeninggal globalMeninggal;

  List<Indonesia> _list = [];
  var loading = false;
  Future<Null> _fetchData() async {
    setState(() {
      loading = true;
    });
    final response = await http.get("https://api.kawalcorona.com/indonesia");
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        for (Map i in data) {
          _list.add(Indonesia.fromJson(i));
        }
        loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchData();
    GlobalSembuh.connectToApiUser('').then((value) {
      setState(() {
        globalSembuh = value;
      });
    });
    GlobalSakit.connectToApiUser('').then((value) {
      setState(() {
        globalSakit = value;
      });
    });
    GlobalMeninggal.connectToApiUser('').then((value) {
      setState(() {
        globalMeninggal = value;
      });
    });
  }

  void showNotification(BuildContext context) {
    Flushbar(
      title: 'Message',
      message: 'Belum Informasi Terbaru',
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Colors.blue.shade300,
      ),
      leftBarIndicatorColor: Colors.blue.shade300,
      duration: Duration(seconds: 3),
    )..show(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: loading
            ? Center(child: CircularProgressIndicator())
            : ListView.builder(
                itemCount: _list.length,
                itemBuilder: (context, i) {
                  final x = _list[i];
                  return Container(
                    // padding: EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          //ini
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              ColoredBox(
                                color: Color(0xFF00B0FF),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: <Widget>[
                                    Text(
                                      "DATA GLOBAL COVID-19 ",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17.0,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 100.0,
                                    ),
                                    FlatButton.icon(
                                      onPressed: () =>
                                          showNotification(context),
                                      icon: Icon(
                                        Icons.notifications_active,
                                        color: Colors.white,
                                      ),
                                      label: Text(""),
                                    ),
                                  ],
                                ),
                              ),
                              ColoredBox(
                                color: Color(0xFF00B0FF),
                                child: Card(
                                  elevation: 0.0,
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Text(
                                            (globalSakit != null)
                                                ? globalSakit.name
                                                : "Loading",
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Text(
                                            (globalSembuh != null)
                                                ? globalSembuh.name
                                                : "Loading",
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Text(
                                            (globalMeninggal != null)
                                                ? globalMeninggal.name
                                                : "Loading",
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          FlatButton.icon(
                                            // onPressed: () {
                                            //   Navigator.of(context).push(MaterialPageRoute(
                                            //     builder: (context) => Topup(),
                                            //   ));
                                            // },
                                            icon: SvgPicture.asset(
                                              'assets/virus.svg',
                                              height: 20.0,
                                              width: 20.0,
                                            ),
                                            label: Text(
                                              (globalSakit != null)
                                                  ? globalSakit.value
                                                  : "Loading",
                                              style: TextStyle(
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          FlatButton.icon(
                                            onPressed: null,
                                            icon: SvgPicture.asset(
                                              'assets/virus.svg',
                                              height: 20.0,
                                              width: 20.0,
                                            ),
                                            label: Text(
                                              (globalSembuh != null)
                                                  ? globalSembuh.value
                                                  : "Loading",
                                              style: TextStyle(
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                          FlatButton.icon(
                                            onPressed: null,
                                            icon: SvgPicture.asset(
                                              'assets/virus.svg',
                                              height: 20.0,
                                              width: 20.0,
                                            ),
                                            label: Text(
                                              (globalMeninggal != null)
                                                  ? globalMeninggal.value
                                                  : "Loading",
                                              style: TextStyle(
                                                color: Colors.grey,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              ColoredBox(
                                color: Color(0xFF00B0FF),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  color: Colors.pink,
                                  elevation: 10,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      ListTile(
                                        leading: SvgPicture.asset(
                                          'assets/indonesia.svg',
                                          height: 150.0,
                                          width: 150.0,
                                        ),
                                        title: Text('Kasus Covid-19',
                                            style:
                                                TextStyle(color: Colors.white)),
                                        subtitle: Text('Indonesia',
                                            style:
                                                TextStyle(color: Colors.white)),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Text(
                                            (globalSakit != null)
                                                ? globalSakit.name
                                                : "Loading",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Text(
                                            (globalSembuh != null)
                                                ? globalSembuh.name
                                                : "Loading",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Text(
                                            (globalMeninggal != null)
                                                ? globalMeninggal.name
                                                : "Loading",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          Text(
                                            (x != null) ? x.positif : "Loading",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Text(
                                            (x != null) ? x.sembuh : "Loading",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 5.0,
                                          ),
                                          Text(
                                            (x != null)
                                                ? x.meninggal
                                                : "Loading",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 17.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        children: <Widget>[
                                          SizedBox(
                                            height: 20.0,
                                            width: 20.0,
                                            child: const DecoratedBox(
                                              decoration: const BoxDecoration(),
                                            ),
                                          ),
                                        ],
                                      ),

                                      // ButtonTheme.bar(
                                      //   child: ButtonBar(
                                      //     children: <Widget>[
                                      //       FlatButton(
                                      //         child: const Text('Edit',
                                      //             style: TextStyle(color: Colors.white)),
                                      //         onPressed: () {},
                                      //       ),
                                      //       FlatButton(
                                      //         child: const Text('Delete',
                                      //             style: TextStyle(color: Colors.white)),
                                      //         onPressed: () {},
                                      //       ),
                                      //     ],
                                      //   ),
                                      // ),
                                    ],
                                  ),
                                ),
                              ),
                              // Align(
                              //   alignment: Alignment.bottomCenter,
                              //   child: Card(
                              //     elevation: 0.0,
                              //     child: Row(
                              //       mainAxisAlignment:
                              //           MainAxisAlignment.spaceAround,
                              //       children: <Widget>[
                              //         Image.asset(
                              //           'assets/ads.png',
                              //           height: 250.0,
                              //           width: 400.0,
                              //         ),
                              //       ],
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                        ColoredBox(
                          color: Color(0xFF00B0FF),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            elevation: 10.0,
                            child: Column(
                              children: <Widget>[
                                Center(
                                    child: Container(
                                        padding:
                                            EdgeInsets.fromLTRB(20, 20, 20, 20),
                                        child: Text(
                                            'Informasi Seputar Covid-19',
                                            style: TextStyle(fontSize: 22)))),
                                ExpansionTile(
                                  title: Text(
                                    "Apa itu Novel Coronavirus (2019-nCoV)?",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        'Novel coronavirus (2019-nCoV) adalah jenis baru coronavirus yang belum pernah diidentifikasi sebelumnya pada manusia. Coronavirus merupakan keluarga besar virus yang menyebabkan penyakit pada manusia dan hewan. Pada manusia menyebabkan penyakit mulai flu biasa hingga penyakit yang serius seperti Middle East Respiratory Syndrome (MERS) dan Sindrom Pernapasan Akut Berat/ Severe Acute Respiratory Syndrome (SARS).\n\nPada 11 Februari 2020, World Health Organization (WHO) mengumumkan nama penyakit yang disebabkan 2019-nCov, yaitu Coronavirus Disease (COVID-19).',
                                        style: TextStyle(),
                                        textAlign: TextAlign.justify,
                                      ),
                                    ),
                                  ],
                                ),
                                ExpansionTile(
                                  title: Text(
                                    "Bagaimana cara mencegah penularan COVID-19?",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  children: <Widget>[
                                    ListTile(
                                      title: Linkify(
                                        onOpen: (link) {
                                          print("Linkify link = ${link.url}");
                                        },
                                        text:
                                            "Untuk Informasi Lbeih Lanjut Kunjungi -  https://pikobar.jabarprov.go.id/faq",
                                        style: TextStyle(color: Colors.blue),
                                        linkStyle:
                                            TextStyle(color: Colors.green),
                                      ),
                                    ),
                                  ],
                                ),
                                ExpansionTile(
                                  title: Text(
                                    "Apa saja gejala COVID-19?",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        'Gejala umum berupa demam ≥38C, batuk, pilek, nyeri tenggorokan dan sesak napas. Jika ada orang dengan gejala tersebut pernah melakukan perjalanan ke Tiongkok (terutama Wuhan), atau pernah merawat/kontak dengan penderita COVID-19, maka terhadap orang tersebut akan dilakukan pemeriksaan laboratorium lebih lanjut untuk memastikan diagnosisnya.',
                                        style: TextStyle(),
                                        textAlign: TextAlign.justify,
                                      ),
                                    ),
                                  ],
                                ),
                                ExpansionTile(
                                  title: Text(
                                    "Seberapa bahaya COVID-19 ini?",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        'Seperti penyakit pernapasan lainnya, infeksi 2019-nCoV dapat menyebabkan gejala ringan termasuk pilek, sakit tenggorokan, batuk, dan demam. Beberapa orang mungkin akan menderita sakit yang parah, seperti disertai pneumonia atau kesulitan bernafas. Walaupun fatalitas penyakit ini masih jarang, namun bagi orang yang berusia lanjut, dan orang-orang dengan kondisi medis yang sudah ada sebelumnya (seperti, diabetes dan penyakit jantung), mereka biasanya lebih rentan untuk menjadi sakit parah.',
                                        style: TextStyle(),
                                        textAlign: TextAlign.justify,
                                      ),
                                    ),
                                  ],
                                ),
                                ExpansionTile(
                                  title: Text(
                                    "Bagaimana manusia bisa terinfeksi COVID-19?",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  children: <Widget>[
                                    ListTile(
                                      title: Text(
                                        'Sampai saat ini, belum diketahui bagaimana manusia bisa terinfeksi virus ini. Para ahli masih sedang melakukan penyelidikan untuk menentukan sumber virus, jenis paparan, cara penularan dan pola klinis serta perjalanan penyakit. Hasil penyelidikan sementara dari beberapa institusi di kota Wuhan, sebagian kasus terjadi pada orang yang bekerja di pasar hewan/ikan, namun belum dapat dipastikan jenis hewan penular virus ini. Hingga saat ini dilaporkan adanya penularan antar manusia yang terbatas (antar keluarga dekat dan petugas kesehatan yang merawat kasus).',
                                        style: TextStyle(),
                                        textAlign: TextAlign.justify,
                                      ),
                                    ),
                                  ],
                                ),
                                ExpansionTile(
                                  title: Text(
                                    "Siapa saja yang berisiko terinfeksi COVID-19?",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  children: <Widget>[
                                    ListTile(
                                      title: Linkify(
                                        onOpen: (link) {
                                          print("Linkify link = ${link.url}");
                                        },
                                        text:
                                            "Untuk Informasi Lbeih Lanjut Kunjungi -  https://pikobar.jabarprov.go.id/faq",
                                        style: TextStyle(color: Colors.blue),
                                        linkStyle:
                                            TextStyle(color: Colors.green),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
      ),
    );
  }
}

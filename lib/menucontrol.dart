import 'package:flutter/material.dart';
import 'package:kang_ipin/maindashboard.dart';
import 'package:kang_ipin/propinsi.dart';
import 'package:kang_ipin/logout.dart';
import 'package:kang_ipin/auth_services.dart';

class Menucontrol extends StatefulWidget {
  @override
  _MenucontrolState createState() => _MenucontrolState();
}

class _MenucontrolState extends State<Menucontrol> {
  int _selectedIndex = 0;
  final List<Widget> _widgetOptions = [
    Maindashboard(),
    Propinsi(),
    Logout(),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.question_answer_outlined),
            label: 'Data Indonesia',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Account',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xFF00B0FF),
        onTap: _onItemTapped,
      ),
    );
  }
}

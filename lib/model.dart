import 'package:http/http.dart' as http;
import 'dart:convert';

class GlobalSembuh {
  String name;
  String value;

  GlobalSembuh({this.name, this.value});

  factory GlobalSembuh.createGlobalSembuh(Map<String, dynamic> object) {
    return GlobalSembuh(name: object['name'], value: object['value']);
  }

  static Future<GlobalSembuh> connectToApiUser(String id) async {
    String apiURLPOST = 'https://api.kawalcorona.com/sembuh/';

    var apiResult = await http.get(apiURLPOST);

    var jsonObject = json.decode(apiResult.body);

    // ignore: non_constant_identifier_names
    var DataSembuh = (jsonObject as Map<String, dynamic>);

    // print(UserData);
    return GlobalSembuh.createGlobalSembuh(DataSembuh);
  }
}

class GlobalSakit {
  String name;
  String value;

  GlobalSakit({this.name, this.value});

  factory GlobalSakit.createGlobalSakit(Map<String, dynamic> object) {
    return GlobalSakit(name: object['name'], value: object['value']);
  }

  static Future<GlobalSakit> connectToApiUser(String id) async {
    String apiURLPOST = 'https://api.kawalcorona.com/positif/';

    var apiResult = await http.get(apiURLPOST);

    var jsonObject = json.decode(apiResult.body);

    // ignore: non_constant_identifier_names
    var DataSakit = (jsonObject as Map<String, dynamic>);

    // print(UserData);
    return GlobalSakit.createGlobalSakit(DataSakit);
  }
}

class GlobalMeninggal {
  String name;
  String value;

  GlobalMeninggal({this.name, this.value});

  factory GlobalMeninggal.createGlobalMeninggal(Map<String, dynamic> object) {
    return GlobalMeninggal(name: object['name'], value: object['value']);
  }

  static Future<GlobalMeninggal> connectToApiUser(String id) async {
    String apiURLPOST = 'https://api.kawalcorona.com/meninggal/';

    var apiResult = await http.get(apiURLPOST);

    var jsonObject = json.decode(apiResult.body);

    // ignore: non_constant_identifier_names
    var DataMeninggal = (jsonObject as Map<String, dynamic>);

    // print(UserData);
    return GlobalMeninggal.createGlobalMeninggal(DataMeninggal);
  }
}

class Indonesia {
  final String name;
  final String positif;
  final String sembuh;
  final String meninggal;

  Indonesia({this.name, this.positif, this.sembuh, this.meninggal});

  factory Indonesia.fromJson(Map<String, dynamic> json) {
    return new Indonesia(
        name: json['name'],
        positif: json['positif'],
        sembuh: json['sembuh'],
        meninggal: json['meninggal']);
  }
}

class Detail {
  final Address kasusindonesia;

  Detail({this.kasusindonesia});

  factory Detail.fromJson(Map<String, dynamic> json) {
    return new Detail(kasusindonesia: Address.fromJson(json['attributes']));
  }
}

class Address {
  final String provinsi;
  final int positif;
  final int sembuh;
  final int meninggal;

  Address({this.provinsi, this.positif, this.sembuh, this.meninggal});

  factory Address.fromJson(Map<String, dynamic> json) {
    return new Address(
        provinsi: json['Provinsi'],
        positif: json['Kasus_Posi'],
        sembuh: json['Kasus_Semb'],
        meninggal: json['Kasus_Meni']);
  }
}

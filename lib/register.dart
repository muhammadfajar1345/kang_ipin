import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kang_ipin/login.dart';
import 'package:kang_ipin/auth_services.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  TextEditingController _confirmPassController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String txtUsername;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF00B0FF),
        body: ListView(children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 50.0,
              ),
              Text(
                "REGISTER ACCOUNT",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                ),
              ),
              Text(
                "Indonesia Peka Informasi Covid-19",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 13.0,
                ),
              ),
              SizedBox(
                height: 55.0,
              ),
              SvgPicture.asset(
                'assets/login.svg',
                height: 150.0,
                width: 150.0,
              ),
              SizedBox(
                height: 55.0,
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      controller: _emailController,
                      validator: (val) {
                        if (val.isEmpty) {
                          return 'Email tidak boleh kosong';
                        } else {
                          return null;
                        }
                      },
                      style: TextStyle(
                        color: Color(0xFF00B0FF),
                      ),
                      maxLength: 35,
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding: EdgeInsets.all(16),
                        border: OutlineInputBorder(),
                        labelText: 'Email',
                        labelStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        prefixIcon: Icon(
                          Icons.account_circle,
                          color: Color(0xFF00B0FF),
                        ),
                        helperText: "Enter Your Username",
                        helperStyle: TextStyle(
                          color: Colors.white,
                        ),
                        disabledBorder: OutlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    TextFormField(
                      controller: _passController,
                      validator: (val) {
                        if (val.isEmpty) {
                          return 'Password tidak boleh kosong';
                        } else {
                          return null;
                        }
                      },
                      style: TextStyle(
                        color: Color(0xFF00B0FF),
                      ),
                      maxLength: 15,
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding: EdgeInsets.all(5),
                        border: OutlineInputBorder(),
                        labelText: 'Password',
                        labelStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        prefixIcon: Icon(
                          Icons.vpn_key_rounded,
                          color: Color(0xFF00B0FF),
                        ),
                        helperText: "Enter Your Password",
                        helperStyle: TextStyle(
                          color: Colors.white,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(20.0),
                          ),
                        ),
                      ),
                      obscureText: true,
                    ),
                    TextFormField(
                      controller: _confirmPassController,
                      validator: (val) {
                        if (val.isEmpty) {
                          return 'Confirm Password tidak boleh kosong';
                        } else if (val != _passController.text) {
                          return 'Password dan Confirm Password harus sama';
                        } else {
                          return null;
                        }
                      },
                      style: TextStyle(
                        color: Color(0xFF00B0FF),
                      ),
                      maxLength: 15,
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        contentPadding: EdgeInsets.all(5),
                        border: OutlineInputBorder(),
                        labelText: 'Confirm Password',
                        labelStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        prefixIcon: Icon(
                          Icons.vpn_key_rounded,
                          color: Color(0xFF00B0FF),
                        ),
                        helperText: "Re-Enter Your Password",
                        helperStyle: TextStyle(
                          color: Colors.white,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(20.0),
                          ),
                        ),
                      ),
                      obscureText: true,
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    FlatButton(
                      minWidth: 240,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                          side: BorderSide(color: Colors.white)),
                      child: Text(
                        'REGISTER',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      color: Colors.white,
                      textColor: Color(0xFF00B0FF),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          SignInSignUpResult result =
                              await AuthServices.createUser(
                                  email: _emailController.text,
                                  pass: _passController.text);

                          if (result.user != null) {
                            // Go to Profile Page
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AlertDialog(
                                  title: Text('Pendaftaran Akun Berhasil'),
                                  content: Text('Pendaftaran Akun ' +
                                      _emailController.text +
                                      ' Berhasil'),
                                  actions: <Widget>[
                                    FlatButton(
                                      onPressed: () {
                                        Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                            builder: (_) {
                                              return LoginPage();
                                            },
                                          ),
                                        );
                                      },
                                      child: Text('OK'),
                                    )
                                  ],
                                ),
                              ),
                            );
                          } else {
                            // Show Dialog
                            showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                      title: Text('Error'),
                                      content: Text(result.message),
                                      actions: <Widget>[
                                        FlatButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          child: Text('OK'),
                                        )
                                      ],
                                    ));
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "UAS Version - Semester 5.0",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 10.0,
                ),
              ),
            ],
          ),
        ]));
  }
}
